//--------------------------------------------------------------------------------------
// File: main.cpp
//
// The main file containing the entry point main().
//--------------------------------------------------------------------------------------

#include <sstream>
#include <iomanip>
#include <random>
#include <iostream>

//DirectX includes
#include <DirectXMath.h>
using namespace DirectX;

// Effect framework includes
#include <d3dx11effect.h>

// DXUT includes
#include <DXUT.h>
#include <DXUTcamera.h>

// DirectXTK includes
#include "Effects.h"
#include "VertexTypes.h"
#include "PrimitiveBatch.h"
#include "GeometricPrimitive.h"
#include "ScreenGrab.h"

// AntTweakBar includes
#include "AntTweakBar.h"

// Internal includes
#include "util/util.h"
#include "util/FFmpeg.h"

// Own includes
#include "RigidBody.h"
#include "collisionDetect.h"

// DXUT camera
// NOTE: CModelViewerCamera does not only manage the standard view transformation/camera position 
//       (CModelViewerCamera::GetViewMatrix()), but also allows for model rotation
//       (CModelViewerCamera::GetWorldMatrix()). 
//       Look out for CModelViewerCamera::SetButtonMasks(...).
CModelViewerCamera g_camera;

// Effect corresponding to "effect.fx"
ID3DX11Effect* g_pEffect = nullptr;

// Main tweak bar
TwBar* g_pTweakBar;

// DirectXTK effects, input layouts and primitive batches for different vertex types
BasicEffect*                               g_pEffectPositionColor               = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionColor          = nullptr;
PrimitiveBatch<VertexPositionColor>*       g_pPrimitiveBatchPositionColor       = nullptr;
																				
BasicEffect*                               g_pEffectPositionNormal              = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionNormal         = nullptr;
PrimitiveBatch<VertexPositionNormal>*      g_pPrimitiveBatchPositionNormal      = nullptr;
																				
BasicEffect*                               g_pEffectPositionNormalColor         = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionNormalColor    = nullptr;
PrimitiveBatch<VertexPositionNormalColor>* g_pPrimitiveBatchPositionNormalColor = nullptr;

// List all objects
std::vector<RigidBody::RigidBody> g_RigidBodies;

// Vectors for picking
XMVECTOR g_v1;
XMVECTOR g_v2;

// Movable object management
XMINT2   g_viMouseDelta = XMINT2(0,0);
XMFLOAT3 g_vfMovableObjectPos = XMFLOAT3(0,0,0);

// TweakAntBar GUI variables
float g_fGravity =  0.0f;
int g_iActiveObject = 0;
int g_iActivePoint = 0;
bool g_bDrawPoints = false;
bool g_bApplyForceOnPoints = false;
int g_iSetup = 0;
int g_iOldSetup = 0;

// Other global variables
float g_fDeltaTime = 1 /128.0f;
float g_fTimeAcc = 0.0f;
float g_Time = 0.0f;
bool g_bNextStep = false;
XMVECTOR g_collisionPosition = XMVectorZero();

// Video recorder
FFmpeg* g_pFFmpegVideoRecorder = nullptr;

// Function prototypes
void InitRigidBodies(ID3D11DeviceContext* pd3dImmediateContext);

// Create TweakBar and add required buttons and variables
void InitTweakBar(ID3D11Device* pd3dDevice)
{
	std::string maxAcitveObject = "min=0 max=";
	maxAcitveObject.append( std::to_string( g_RigidBodies.size() - 1 ) );


	TwInit(TW_DIRECT3D11, pd3dDevice);

	g_pTweakBar = TwNewBar("TweakBar");

	//ID3D11DeviceContext* pd3dImmediateContext;
	//pd3dDevice->GetImmediateContext(&pd3dImmediateContext);

	// HINT: For buttons you can directly pass the callback function as a lambda expression.
	TwAddButton(g_pTweakBar, "Reset Camera", [](void *){g_camera.Reset(); }, nullptr, "");
	TwAddButton(g_pTweakBar, "Reset Rigid bodies", [](void * context){ 
			InitRigidBodies((ID3D11DeviceContext*)context);  
			printf("Reset current setup\n"); }, DXUTGetD3D11DeviceContext(), "");
	TwAddVarRW( g_pTweakBar, "Setup", TW_TYPE_INT16, &g_iSetup, "min=0 max=2" );
	TwAddVarRW( g_pTweakBar, "Gravity", TW_TYPE_FLOAT, &g_fGravity, "step=0.01" );
	TwAddVarRW( g_pTweakBar, "Active Object", TW_TYPE_INT16, &g_iActiveObject, maxAcitveObject.c_str() );
	TwAddVarRW( g_pTweakBar, "Active Point", TW_TYPE_INT16, &g_iActivePoint, "min=0 max=7" );
	TwAddVarRW( g_pTweakBar, "Draw Points", TW_TYPE_BOOLCPP, &g_bDrawPoints, "" );
	TwAddVarRW( g_pTweakBar, "Apply Force to ", TW_TYPE_BOOLCPP, &g_bApplyForceOnPoints, "true='Points' false='Object'" );
	//TwAddVarRW( g_pTweakBar, "Body/Point Mass", TW_TYPE_FLOAT, &g_fActivePointMass, "min=0.1 step=0.1" );
	
}

void InitRigidBodies(ID3D11DeviceContext* pd3dImmediateContext)
{
	// Clear everything first
	for (auto& it : g_RigidBodies)
	{
		SAFE_DELETE(it.pGeoObject_);
	}
	
	g_RigidBodies.clear();

	XMMATRIX rotation = XMMatrixRotationX(XM_PI * 0.20f) * XMMatrixRotationY(XM_PI * 0.25f);

	
	switch (g_iSetup)
	{
	case 0:
		g_fGravity = 0.0f;
		g_RigidBodies.emplace_back(1.0f, 1.0f, 1.0f, GeometricPrimitive::CreateCube(pd3dImmediateContext, 1.0f, false).release());
		g_RigidBodies[0].Initialize(XMVectorSet(0, 2, 0, 0), XMVectorSet(0, 0, 0, 0), rotation, XMVectorSet(0, 0, 0, 0));
		break;

	case 1:
		g_fGravity = 0.0f;
		g_RigidBodies.emplace_back(1.0f, 1.0f, 1.0f, GeometricPrimitive::CreateCube(pd3dImmediateContext, 1.0f, false).release());
		g_RigidBodies.emplace_back(1.0f, 1.0f, 1.0f, GeometricPrimitive::CreateCube(pd3dImmediateContext, 1.0f, false).release());
		g_RigidBodies[0].Initialize(XMVectorSet(-2, 5, 0, 0), XMVectorSet(1, 0, 0, 0), XMMatrixIdentity(), XMVectorSet(0, 0, 0, 0));
		g_RigidBodies[1].Initialize(XMVectorSet(2, 5, 0, 0), XMVectorSet(-1, 0, 0, 0), rotation, XMVectorSet(0, 0, 0, 0));
		break;

	case 2:
		g_fGravity = 0.0;
		// Floor
		//g_RigidBodies.emplace_back(10.0f, 1.0f, 10.0f, GeometricPrimitive::CreateCube(pd3dImmediateContext, 1.0f, false).release(), true);
		//g_RigidBodies[0].Initialize(XMVectorSet(0.0f, -3.0f, 0.0f, 0.0f), XMVectorSet(0, 0, 0, 0), XMMatrixIdentity(), XMVectorSet(0, 0, 0, 0));

		for (int i = 0; i < 2; i++)
		{
			for (int j = 0; j < 2; j++)
			{
				g_RigidBodies.emplace_back(1.0f, 1.0f, 1.0f, GeometricPrimitive::CreateCube(pd3dImmediateContext, 1.0f, false).release());
				g_RigidBodies[i*2+j].Initialize(XMVectorSet(-2.0f + i*1.4f, -1.0f + 1.4f*j, 0.0f, 0.0f), XMVectorSet(0, 0, 0, 0), XMMatrixIdentity(), XMVectorSet(0, 0, 0, 0));
			}
		}

		g_RigidBodies.emplace_back(1.0f, 1.0f, 1.0f, GeometricPrimitive::CreateCube(pd3dImmediateContext, 1.0f, false).release(), false);
		g_RigidBodies[4].Initialize(XMVectorSet(-1.0f, 0.0f, -4.0f, 0.0f), XMVectorSet(0, 0, 3, 0), XMMatrixIdentity(), XMVectorSet(-2, 0, -1, 0));

		break;
	}
	/*
	// Floor
	g_RigidBodies.emplace_back(100.0f,1.0f, 100.0f, GeometricPrimitive::CreateCube(pd3dImmediateContext, 1.0f, false).release(), true);

	// Setup 1
	g_RigidBodies.emplace_back( 1.0f,1.0f,1.0f, GeometricPrimitive::CreateCube( pd3dImmediateContext, 1.0f, false ).release() );
	g_RigidBodies.emplace_back( 1.0f, 1.0f, 1.0f, GeometricPrimitive::CreateCube( pd3dImmediateContext, 1.0f, false ).release() );

	g_RigidBodies[0].Initialize(XMVectorSet(0.0f, -3.0f, 0.0f, 0.0f), XMVectorSet(0, 0, 0, 0), XMMatrixIdentity() /*rotation, XMVectorSet(0, 0, 0, 0));



	g_RigidBodies[1].Initialize(XMVectorSet(0, 5, 0, 0), XMVectorSet(0, 0, 0, 0), rotation, XMVectorSet(0, 0, 0, 0));
	g_RigidBodies[2].Initialize(XMVectorSet(2, 5, 0, 0), XMVectorSet(-1, 0, 0, 0),  rotation, XMVectorSet(0, 0, 0, 0));
	*/
	for (auto& it : g_RigidBodies)
	{
		it.SetExternalForce(XMVectorSet(0, g_fGravity, 0, 0));
	}
}


void DrawRigidBodies( ID3D11DeviceContext* pd3dImmediateContext )
{
	int i = 0;

	//std::unique_ptr<GeometricPrimitive> collision = GeometricPrimitive::CreateSphere(pd3dImmediateContext,0.7f, 3, false);
	//g_pEffectPositionNormal->SetWorld(XMMatrixTranslationFromVector(g_collisionPosition) * g_camera.GetWorldMatrix());
	//collision->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
	
	for ( auto& it : g_RigidBodies )
	{
		//if (it.isFixed_) continue;
		
		// Highlight the active object
		if ( i == g_iActiveObject )
		{
			g_pEffectPositionNormal->SetDiffuseColor( 0.6f *  XMVectorSet( 1, 1, 0, 0 )  );			
		}
		else
		{
			g_pEffectPositionNormal->SetDiffuseColor( 0.6f *  XMVectorSet( 0,1, 1, 0 ) );
		}
		
		XMMATRIX scale = XMMatrixScaling(it.fWidth_, it.fHeight_, it.fDepth_);
		XMMATRIX trans = XMMatrixTranslationFromVector( it.position_ );

		XMMATRIX rot = XMMatrixRotationQuaternion( it.orientation_ );
		g_pEffectPositionNormal->SetWorld( scale * rot  *trans * g_camera.GetWorldMatrix() );


		it.pGeoObject_->Draw( g_pEffectPositionNormal, g_pInputLayoutPositionNormal );

		if ( g_bDrawPoints == true )
		{
			int j = 0;
			for ( auto& jt : it.points_ )
			{

				std::unique_ptr<GeometricPrimitive> point = GeometricPrimitive::CreateSphere(pd3dImmediateContext, 0.2f, 3, false);
				if (j == g_iActivePoint && i == g_iActiveObject)
				{
					g_pEffectPositionNormal->SetDiffuseColor(0.2f *  XMVectorSet(1, 1, 1, 0));
				}
				else
				{
					g_pEffectPositionNormal->SetDiffuseColor(0.6f *  XMVectorSet(1, 0, 0, 0));
				}

				g_pEffectPositionNormal->SetWorld(XMMatrixTranslationFromVector(jt.worldPosition_) * g_camera.GetWorldMatrix());
				point->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
				j++;
			}
		}
		i++;
	}
}

// Draw a large, square plane at y=-1 with a checkerboard pattern
// (Drawn as multiple quads, i.e. triangle strips, using a DirectXTK primitive batch)
void DrawFloor(ID3D11DeviceContext* pd3dImmediateContext)
{
	// Setup position/normal/color effect
	g_pEffectPositionNormalColor->SetWorld(XMMatrixIdentity());
	g_pEffectPositionNormalColor->SetEmissiveColor(Colors::Black);
	g_pEffectPositionNormalColor->SetDiffuseColor(0.8f * Colors::White);
	g_pEffectPositionNormalColor->SetSpecularColor(0.4f * Colors::White);
	g_pEffectPositionNormalColor->SetSpecularPower(1000);

	g_pEffectPositionNormalColor->Apply(pd3dImmediateContext);
	pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionNormalColor);

	// Draw 4*n*n quads spanning x = [-n;n], y = -1, z = [-n;n]
	const float n = 4;
	XMVECTOR normal = XMVectorSet(0, 1, 0, 0);
	XMVECTOR planecenter = XMVectorSet(0, -0.5f, 0, 0);

	g_pPrimitiveBatchPositionNormalColor->Begin();
	for (float z = -n; z < n; z++)
	{
		for (float x = -n; x < n; x++)
		{
			// Quad vertex positions
			XMVECTOR pos[] = { XMVectorSet(x, -0.5f, z + 1, 0),
				XMVectorSet(x + 1, -0.5f, z + 1, 0),
				XMVectorSet(x + 1, -0.5f, z, 0),
				XMVectorSet(x, -0.5f, z, 0) };

			// Color checkerboard pattern (white & gray)
			XMVECTOR color = ((int(z + x) % 2) == 0) ? XMVectorSet(1, 1, 1, 1) : XMVectorSet(0.6f, 0.6f, 0.6f, 1);

			// Color attenuation based on distance to plane center
			float attenuation[] = {
				1.0f - XMVectorGetX(XMVector3Length(pos[0] - planecenter)) / n,
				1.0f - XMVectorGetX(XMVector3Length(pos[1] - planecenter)) / n,
				1.0f - XMVectorGetX(XMVector3Length(pos[2] - planecenter)) / n,
				1.0f - XMVectorGetX(XMVector3Length(pos[3] - planecenter)) / n };

			g_pPrimitiveBatchPositionNormalColor->DrawQuad(
				VertexPositionNormalColor(pos[0], normal, attenuation[0] * color),
				VertexPositionNormalColor(pos[1], normal, attenuation[1] * color),
				VertexPositionNormalColor(pos[2], normal, attenuation[2] * color),
				VertexPositionNormalColor(pos[3], normal, attenuation[3] * color)
				);
		}
	}
	g_pPrimitiveBatchPositionNormalColor->End();
}

void Impulse(RigidBody::RigidBody& a, RigidBody::RigidBody& b, CollisionInfo info) {
 	
	XMVECTOR vA, vB, xA, xB;
	XMVECTOR vRel;

	// Arbitrary collision point
	if (!info.isEdge && !info.isFace) {
		vA = a.velocityOld_ + XMVector3Cross(a.angularVelocity_, info.collisionPointWorld [0]);
		vB = b.velocityOld_ + XMVector3Cross(b.angularVelocity_, info.collisionPointWorld [0]);

		xA = info.collisionPointWorld [0] - a.position_;
		xB = info.collisionPointWorld [0] - b.position_;

		vRel = vA - vB;
	}
	// Cube colliding with a whole face
	else if (info.isFace) {
		XMVECTOR midPoint = XMVectorZero();
		for (int i = 0; i < 4; ++i) {
			midPoint += info.collisionPointWorld[i];
		}
		midPoint /= 4;

		XMVECTOR n = XMVector3Cross(
			XMVectorSubtract(info.collisionPointWorld [1], info.collisionPointWorld [0]), 
			XMVectorSubtract(info.collisionPointWorld [2], info.collisionPointWorld [0]));
		info.normalWorld = n;

		vA = a.velocityOld_ + XMVector3Cross(a.angularVelocity_, midPoint);
		vB = b.velocityOld_ + XMVector3Cross(b.angularVelocity_, midPoint);

		xA = midPoint - a.position_;
		xB = midPoint - b.position_;

		vRel = vA - vB;

		
	}
	// Cube colliding with edge
	else {
		XMVECTOR midPoint = XMVectorZero();
		for (int i = 0; i < 2; ++i) {
			midPoint += info.collisionPointWorld[i];
		}
		midPoint /= 2;

		vA = a.velocityOld_ + XMVector3Cross(a.angularVelocity_, midPoint);
		vB = b.velocityOld_ + XMVector3Cross(b.angularVelocity_, midPoint);

		xA = midPoint - a.position_;
		xB = midPoint - b.position_;

		vRel = vA - vB;
	}

	if (XMVectorGetX((XMVector3Dot(vRel, info.normalWorld))) < 0)
	{
		float j = XMVectorGetX((XMVector3Dot(-(1 + 0.2f) * vRel, info.normalWorld))) /
			(a.fMassInverse_ + b.fMassInverse_ +
			XMVectorGetX(XMVector3Dot((
			XMVector3Cross(
			XMVector4Transform(XMVector3Cross(
			xA, info.normalWorld), a.inertiaTensorInverse_), xA)
			+
			XMVector3Cross(
			XMVector4Transform(XMVector3Cross(
			xB, info.normalWorld), b.inertiaTensorInverse_), xB)
			), info.normalWorld)));

		a.velocity_ = a.velocity_ + j * info.normalWorld * a.fMassInverse_;
		b.velocity_ = b.velocity_ - j * info.normalWorld * b.fMassInverse_;

		printf("A: %f %f %f -> %f %f %f\n", XMVectorGetX(a.velocityOld_), XMVectorGetY(a.velocityOld_), XMVectorGetZ(a.velocityOld_),
			XMVectorGetX(a.velocity_), XMVectorGetY(a.velocity_), XMVectorGetZ(a.velocity_));

		printf("B: %f %f %f -> %f %f %f\n", XMVectorGetX(b.velocityOld_), XMVectorGetY(b.velocityOld_), XMVectorGetZ(b.velocityOld_),
			XMVectorGetX(b.velocity_), XMVectorGetY(b.velocity_), XMVectorGetZ(b.velocity_));

		a.torqueAccumulator_ = a.torqueAccumulator_ + XMVector3Cross(
			xA, j * info.normalWorld);

		b.torqueAccumulator_ = b.torqueAccumulator_ - XMVector3Cross(
			xB, j * info.normalWorld);

	}
}

// ============================================================
// DXUT Callbacks
// ============================================================

//--------------------------------------------------------------------------------------
// Reject any D3D11 devices that aren't acceptable by returning false
//--------------------------------------------------------------------------------------
bool CALLBACK IsD3D11DeviceAcceptable( const CD3D11EnumAdapterInfo *AdapterInfo, UINT Output, const CD3D11EnumDeviceInfo *DeviceInfo,
									   DXGI_FORMAT BackBufferFormat, bool bWindowed, void* pUserContext )
{
	return true;
}

//--------------------------------------------------------------------------------------
// Called right before creating a device, allowing the app to modify the device settings as needed
//--------------------------------------------------------------------------------------
bool CALLBACK ModifyDeviceSettings( DXUTDeviceSettings* pDeviceSettings, void* pUserContext )
{
	return true;
}

//--------------------------------------------------------------------------------------
// Create any D3D11 resources that aren't dependent on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11CreateDevice( ID3D11Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
	HRESULT hr;

	ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();;

	std::wcout << L"Device: " << DXUTGetDeviceStats() << std::endl;
	
	// Load custom effect from "effect.fxo" (compiled "effect.fx")
	std::wstring effectPath = GetExePath() + L"effect.fxo";
	if(FAILED(hr = D3DX11CreateEffectFromFile(effectPath.c_str(), 0, pd3dDevice, &g_pEffect)))
	{
		std::wcout << L"Failed creating effect with error code " << int(hr) << std::endl;
		return hr;
	}

	InitRigidBodies(pd3dImmediateContext);

	// Init AntTweakBar GUI
	InitTweakBar(pd3dDevice);
	

	// Create DirectXTK geometric primitives for later usage


	// Create effect, input layout and primitive batch for position/color vertices (DirectXTK)
	{
		// Effect
		g_pEffectPositionColor = new BasicEffect(pd3dDevice);
		g_pEffectPositionColor->SetVertexColorEnabled(true); // triggers usage of position/color vertices

		// Input layout
		void const* shaderByteCode;
		size_t byteCodeLength;
		g_pEffectPositionColor->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);
		
		pd3dDevice->CreateInputLayout(VertexPositionColor::InputElements,
									  VertexPositionColor::InputElementCount,
									  shaderByteCode, byteCodeLength,
									  &g_pInputLayoutPositionColor);

		// Primitive batch
		g_pPrimitiveBatchPositionColor = new PrimitiveBatch<VertexPositionColor>(pd3dImmediateContext);
	}

	// Create effect, input layout and primitive batch for position/normal vertices (DirectXTK)
	{
		// Effect
		g_pEffectPositionNormal = new BasicEffect(pd3dDevice);
		g_pEffectPositionNormal->EnableDefaultLighting(); // triggers usage of position/normal vertices
		g_pEffectPositionNormal->SetPerPixelLighting(true);

		// Input layout
		void const* shaderByteCode;
		size_t byteCodeLength;
		g_pEffectPositionNormal->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

		pd3dDevice->CreateInputLayout(VertexPositionNormal::InputElements,
									  VertexPositionNormal::InputElementCount,
									  shaderByteCode, byteCodeLength,
									  &g_pInputLayoutPositionNormal);

		// Primitive batch
		g_pPrimitiveBatchPositionNormal = new PrimitiveBatch<VertexPositionNormal>(pd3dImmediateContext);
	}

	// Create effect, input layout and primitive batch for position/normal/color vertices (DirectXTK)
	{
		// Effect
		g_pEffectPositionNormalColor = new BasicEffect(pd3dDevice);
		g_pEffectPositionNormalColor->SetPerPixelLighting(true);
		g_pEffectPositionNormalColor->EnableDefaultLighting();     // triggers usage of position/normal/color vertices
		g_pEffectPositionNormalColor->SetVertexColorEnabled(true); // triggers usage of position/normal/color vertices

		// Input layout
		void const* shaderByteCode;
		size_t byteCodeLength;
		g_pEffectPositionNormalColor->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

		pd3dDevice->CreateInputLayout(VertexPositionNormalColor::InputElements,
									  VertexPositionNormalColor::InputElementCount,
									  shaderByteCode, byteCodeLength,
									  &g_pInputLayoutPositionNormalColor);

		// Primitive batch
		g_pPrimitiveBatchPositionNormalColor = new PrimitiveBatch<VertexPositionNormalColor>(pd3dImmediateContext);
	}

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11CreateDevice 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11DestroyDevice( void* pUserContext )
{
	SAFE_RELEASE(g_pEffect);
	
	TwDeleteBar(g_pTweakBar);
	g_pTweakBar = nullptr;
	TwTerminate();

	for ( auto& it : g_RigidBodies )
	{
		SAFE_DELETE( it.pGeoObject_ );
	}
	
	SAFE_DELETE (g_pPrimitiveBatchPositionColor);
	SAFE_RELEASE(g_pInputLayoutPositionColor);
	SAFE_DELETE (g_pEffectPositionColor);

	SAFE_DELETE (g_pPrimitiveBatchPositionNormal);
	SAFE_RELEASE(g_pInputLayoutPositionNormal);
	SAFE_DELETE (g_pEffectPositionNormal);

	SAFE_DELETE (g_pPrimitiveBatchPositionNormalColor);
	SAFE_RELEASE(g_pInputLayoutPositionNormalColor);
	SAFE_DELETE (g_pEffectPositionNormalColor);
}

//--------------------------------------------------------------------------------------
// Create any D3D11 resources that depend on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11ResizedSwapChain( ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
										  const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
	// Update camera parameters
	int width = pBackBufferSurfaceDesc->Width;
	int height = pBackBufferSurfaceDesc->Height;
	g_camera.SetWindow(width, height);
	g_camera.SetProjParams(XM_PI / 4.0f, float(width) / float(height), 0.1f, 100.0f);

	// Inform AntTweakBar about back buffer resolution change
	TwWindowSize(pBackBufferSurfaceDesc->Width, pBackBufferSurfaceDesc->Height);

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11ResizedSwapChain 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11ReleasingSwapChain( void* pUserContext )
{
}

//--------------------------------------------------------------------------------------
// Handle key presses
//--------------------------------------------------------------------------------------
void CALLBACK OnKeyboard( UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext )
{
	HRESULT hr;

	if(bKeyDown)
	{
		switch(nChar)
		{
			// RETURN: toggle fullscreen
			case VK_RETURN :
			{
				if(bAltDown) DXUTToggleFullScreen();
				break;
			}
			// F8: Take screenshot
			case VK_F8:
			{
				// Save current render target as png
				static int nr = 0;
				std::wstringstream ss;
				ss << L"Screenshot" << std::setfill(L'0') << std::setw(4) << nr++ << L".png";

				ID3D11Resource* pTex2D = nullptr;
				DXUTGetD3D11RenderTargetView()->GetResource(&pTex2D);
				SaveWICTextureToFile(DXUTGetD3D11DeviceContext(), pTex2D, GUID_ContainerFormatPng, ss.str().c_str());
				SAFE_RELEASE(pTex2D);

				std::wcout << L"Screenshot written to " << ss.str() << std::endl;
				break;
			}
			// F10: Toggle video recording
			case VK_F10:
			{
				if (!g_pFFmpegVideoRecorder) {
					g_pFFmpegVideoRecorder = new FFmpeg(25, 21, FFmpeg::MODE_INTERPOLATE);
					V(g_pFFmpegVideoRecorder->StartRecording(DXUTGetD3D11Device(), DXUTGetD3D11RenderTargetView(), "output.avi"));
				} else {
					g_pFFmpegVideoRecorder->StopRecording();
					SAFE_DELETE(g_pFFmpegVideoRecorder);
				}
			}
			// Space next step
			case VK_SPACE:
				g_bNextStep = !g_bNextStep;
		}
	}
}


//--------------------------------------------------------------------------------------
// Handle mouse button presses
//--------------------------------------------------------------------------------------
void CALLBACK OnMouse( bool bLeftButtonDown, bool bRightButtonDown, bool bMiddleButtonDown,
					   bool bSideButton1Down, bool bSideButton2Down, int nMouseWheelDelta,
					   int xPos, int yPos, void* pUserContext )
{
	// Track mouse movement if left mouse key is pressed
	{
		static int xPosSave = 0, yPosSave = 0;

		if (bLeftButtonDown)
		{
			// Accumulate deltas in g_viMouseDelta
			g_viMouseDelta.x += xPos - xPosSave;
			g_viMouseDelta.y += yPos - yPosSave;
		}    

		xPosSave = xPos;
		yPosSave = yPos;
	}
}


//--------------------------------------------------------------------------------------
// Handle messages to the application
//--------------------------------------------------------------------------------------
LRESULT CALLBACK MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam,
						  bool* pbNoFurtherProcessing, void* pUserContext )
{
	// Send message to AntTweakbar first
	if (TwEventWin(hWnd, uMsg, wParam, lParam))
	{
		*pbNoFurtherProcessing = true;
		return 0;
	}

	// If message not processed yet, send to camera
	if(g_camera.HandleMessages(hWnd,uMsg,wParam,lParam))
	{
		*pbNoFurtherProcessing = true;
		return 0;
	}

	return 0;
}


//--------------------------------------------------------------------------------------
// Handle updates to the scene
//--------------------------------------------------------------------------------------
void CALLBACK OnFrameMove( double dTime, float fElapsedTime, void* pUserContext )
{
	std::wstring title = g_bNextStep ? L"RUNNING" : L"PAUSED";

	UpdateWindowTitle(L"Rigid Body Simulation -" + title + L"- ");

	// Move camera
	g_camera.FrameMove(fElapsedTime);

	g_fTimeAcc += fElapsedTime;
	
	

	while ( g_fTimeAcc > g_fDeltaTime )
	{
		if (g_iOldSetup != g_iSetup)
		{
			g_iOldSetup = g_iSetup;
			InitRigidBodies(DXUTGetD3D11DeviceContext());
			TwTerminate();
			InitTweakBar(DXUTGetD3D11Device());
		}
		
		if ( g_bNextStep == true )
		{
			int i = 0;

			for (auto a = g_RigidBodies.begin(); a != g_RigidBodies.end() - 1; ++a) {
				for (auto b = a + 1; b != g_RigidBodies.end(); ++b) {
					XMMATRIX scaleA = XMMatrixScaling(a->fWidth_, a->fHeight_, a->fDepth_);
					XMMATRIX transA = XMMatrixTranslationFromVector(a->position_);
					XMMATRIX rotA = XMMatrixRotationQuaternion(a->orientation_);

					XMMATRIX scaleB = XMMatrixScaling(b->fWidth_, b->fHeight_, b->fDepth_);
					XMMATRIX transB = XMMatrixTranslationFromVector(b->position_);
					XMMATRIX rotB = XMMatrixRotationQuaternion(b->orientation_);

					XMMATRIX obj2World_A(scaleA * rotA  *transA);
					XMMATRIX obj2Worlb_B(scaleB * rotB  *transB);
					CollisionInfo info = checkCollision(obj2World_A, obj2Worlb_B,
						a->fWidth_, a->fHeight_, a->fDepth_,
						b->fWidth_, b->fHeight_, b->fDepth_);

					if (!info.isValid)
					{
						info = checkCollision(obj2Worlb_B, obj2World_A,
							b->fWidth_, b->fHeight_, b->fDepth_,
							a->fWidth_, a->fHeight_, a->fDepth_);
					}

					if (info.isValid) {
						printf("Hit\n");
						Impulse(*a, *b, info);
					}
				}
			}
			
			for ( auto& it : g_RigidBodies )
			{
				it.Update( g_fDeltaTime );
				it.SetExternalForce( XMVectorSet( 0, g_fGravity, 0, 0 ) );
				
				// Apply forces	to active object						
				if ( i == g_iActiveObject )
				{
					if ( g_bApplyForceOnPoints == true )
					{
						it.AddExternalForce( XMLoadFloat3( &g_vfMovableObjectPos ), g_iActivePoint );
					} else {
						it.AddExternalForce( XMVectorSet( g_vfMovableObjectPos.x, g_vfMovableObjectPos.y, g_vfMovableObjectPos.z, 0 ) );
					}
					g_vfMovableObjectPos = XMFLOAT3( 0, 0, 0 );
				}
				i++;
			}
		}

		g_Time += g_fDeltaTime;
		g_fTimeAcc -= g_fDeltaTime;
	}

	

	// Update effects with new view + proj transformations
	g_pEffectPositionColor      ->SetView      (g_camera.GetViewMatrix());
	g_pEffectPositionColor      ->SetProjection(g_camera.GetProjMatrix());

	g_pEffectPositionNormal     ->SetView      (g_camera.GetViewMatrix());
	g_pEffectPositionNormal     ->SetProjection(g_camera.GetProjMatrix());

	g_pEffectPositionNormalColor->SetView      (g_camera.GetViewMatrix());
	g_pEffectPositionNormalColor->SetProjection(g_camera.GetProjMatrix());

	// Apply accumulated mouse deltas to g_vfMovableObjectPos (move along cameras view plane)
	if (g_viMouseDelta.x != 0 || g_viMouseDelta.y != 0)
	{
		// Calcuate camera directions in world space
		XMMATRIX viewInv = XMMatrixInverse(nullptr, g_camera.GetViewMatrix());
		XMVECTOR camRightWorld = XMVector3TransformNormal(g_XMIdentityR0, viewInv);
		XMVECTOR camUpWorld    = XMVector3TransformNormal(g_XMIdentityR1, viewInv);

		// Add accumulated mouse deltas to movable object pos
		XMVECTOR vMovableObjectPos; // = XMLoadFloat3( &g_vfMovableObjectPos );

		float speedScale;
		if ( g_bApplyForceOnPoints == true )
		{
			speedScale = 1.0f;
		}
		else
		{
			speedScale = 5.0f;
		}

		vMovableObjectPos = XMVectorSet(0, 0, 0, 0);
		vMovableObjectPos = XMVectorAdd(vMovableObjectPos,  speedScale * (float)g_viMouseDelta.x * camRightWorld);
		vMovableObjectPos = XMVectorAdd(vMovableObjectPos, -speedScale * (float)g_viMouseDelta.y * camUpWorld);

		XMStoreFloat3(&g_vfMovableObjectPos, vMovableObjectPos);
		
		// Reset accumulated mouse deltas
		g_viMouseDelta = XMINT2(0,0);
	}
}

//--------------------------------------------------------------------------------------
// Render the scene using the D3D11 device
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11FrameRender( ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext,
								  double fTime, float fElapsedTime, void* pUserContext )
{
	HRESULT hr;

	// Clear render target and depth stencil
	float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

	ID3D11RenderTargetView* pRTV = DXUTGetD3D11RenderTargetView();
	ID3D11DepthStencilView* pDSV = DXUTGetD3D11DepthStencilView();
	pd3dImmediateContext->ClearRenderTargetView( pRTV, ClearColor );
	pd3dImmediateContext->ClearDepthStencilView( pDSV, D3D11_CLEAR_DEPTH, 1.0f, 0 );

	// Draw floor
	//DrawFloor(pd3dImmediateContext);

	DrawRigidBodies(pd3dImmediateContext);

	// Draw GUI
	TwDraw();

	if (g_pFFmpegVideoRecorder) 
	{
		V(g_pFFmpegVideoRecorder->AddFrame(pd3dImmediateContext, DXUTGetD3D11RenderTargetView()));
	}
}

//--------------------------------------------------------------------------------------
// Initialize everything and go into a render loop
//--------------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
#if defined(DEBUG) | defined(_DEBUG)
	// Enable run-time memory check for debug builds.
	// (on program exit, memory leaks are printed to Visual Studio's Output console)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

#ifdef _DEBUG
	std::wcout << L"---- DEBUG BUILD ----\n\n";
#endif

	// Set general DXUT callbacks
	DXUTSetCallbackMsgProc( MsgProc );
	DXUTSetCallbackMouse( OnMouse, true );
	DXUTSetCallbackKeyboard( OnKeyboard );

	DXUTSetCallbackFrameMove( OnFrameMove );
	DXUTSetCallbackDeviceChanging( ModifyDeviceSettings );

	// Set the D3D11 DXUT callbacks
	DXUTSetCallbackD3D11DeviceAcceptable( IsD3D11DeviceAcceptable );
	DXUTSetCallbackD3D11DeviceCreated( OnD3D11CreateDevice );
	DXUTSetCallbackD3D11SwapChainResized( OnD3D11ResizedSwapChain );
	DXUTSetCallbackD3D11FrameRender( OnD3D11FrameRender );
	DXUTSetCallbackD3D11SwapChainReleasing( OnD3D11ReleasingSwapChain );
	DXUTSetCallbackD3D11DeviceDestroyed( OnD3D11DestroyDevice );

	// Init camera
	XMFLOAT3 eye(0.0f, 3.0f, -6.0f);
	XMFLOAT3 lookAt(0.0f, 2.0f, 0.0f);
	g_camera.SetViewParams(XMLoadFloat3(&eye), XMLoadFloat3(&lookAt));
	g_camera.SetButtonMasks(MOUSE_MIDDLE_BUTTON, MOUSE_WHEEL, MOUSE_RIGHT_BUTTON);


	// Init DXUT and create device
	DXUTInit( true, true, NULL ); // Parse the command line, show msgboxes on error, no extra command line params
	//DXUTSetIsInGammaCorrectMode( false ); // true by default (SRGB backbuffer), disable to force a RGB backbuffer
	DXUTSetCursorSettings( true, true ); // Show the cursor and clip it when in full screen
	DXUTCreateWindow( L"Rigid Body Simulation" );
	DXUTCreateDevice( D3D_FEATURE_LEVEL_10_0, true, 800, 600 );
	
	DXUTMainLoop(); // Enter into the DXUT render loop

	DXUTShutdown(); // Shuts down DXUT (includes calls to OnD3D11ReleasingSwapChain() and OnD3D11DestroyDevice())

	return DXUTGetExitCode();
}
